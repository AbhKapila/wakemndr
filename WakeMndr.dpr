program wakemndr;

{ Simulation of wake meandering
   Format as wind INT files
   Coordinate system GL, first vertical, then horizontal }

{$APPTYPE CONSOLE}

{$IFDEF RELEASE}
{$R version.res}
{$ENDIF}
{$R *.res}

uses
  Classes,Math,SysUtils, JsonDataObjects, RegularExpressions;
const
  N2max=16384; (* max antal step i tidsserie *)
  NSmax=2;
  SIM_FILE_EXT='gf.json' ;
type
  N2array=array[1..N2max] of double;
var
  Ran0Y,GasdevGset:single;
  Ran0V:array[1..97] of single;
  GasdevIset:integer;
  Ran3Inext,Ran3Inextp:integer;
  Ran3Ma:array[1..55] of single;
  IntData:array of smallint;
  X, Y, X_nf, Y_nf:N2array;
  idum,N,N2,NA,NS,NP,NU,NDIR,IFAK,Seed:integer;
  D,DF,DT,Lkv,Lkw,Tau,Fc:single;
  RS:array[1..NSmax] of single;
  TEKST:string[40];
  Outfil:string;
  IDstr:string[15];
  F3:file;
  RanType,sD, SimFileName,OutFileName :string;
  sDir,U, Fti_v, Fti_w: double;
  Nsec:integer;

  F4, F5, F6, F7, F8: textfile;
  FilterFileNameX, NoFilterFileNameX,FilterFileNameY, NoFilterFileNameY, freq : string;
  FilterFile, NoFilterFile:string[40];
  X4plot, Y4plot,X_nf4plot, Y_nf4plot, FQ :N2array;

function ran0(var idum:integer):single;
(* Improved random generator with Bays-Durham shuffle (Numerical Recipes)
   returns uniform deviate between 0 and 1.

   Set idum to a negative integer to (re)initialise a sequence.
   (do not reset idum between calls).
   Every integer generates a different sequence.

   Ran0Y and Ran0V are global variables keeping previous values *)
var
  j:integer;
begin
  if idum<0 then begin
    RandSeed := -idum;
    idum := 1;
    for j := 1 to 97 do Ran0V[j] := Random;
    Ran0Y := Random;
  end;
  j := 1+Trunc(97.0*Ran0Y);
  if j>97 then j := 97;
  Ran0Y := Ran0V[j];
  ran0 := Ran0Y;
  Ran0V[j] := Random;
end; (* ran0 *)

function ran3(var idum:integer):single;
const
  mbig=4.0E6;
  mseed=1618033.0;
  mz=0.0;
  fac=2.5E-7;
var
  i,ii,k:integer;
  mj,mk:single;
begin
  if idum<0 then begin
    mj := mseed+idum;
    if mj>=0.0 then
      mj := mj-mbig*trunc(mj/mbig)
    else
      mj := mbig-abs(mj)+mbig*trunc(abs(mj)/mbig);
    Ran3Ma[55] := mj;
    mk := 1;
    for i := 1 to 54 do begin
      ii := 21*i mod 55;
      Ran3Ma[ii] := mk;
      mk := mj-mk;
      if mk<mz then mk := mk+mbig;
      mj := Ran3Ma[ii]
    end;
    for k := 1 to 4 do begin
      for i := 1 to 55 do begin
        Ran3Ma[i] := Ran3Ma[i]-Ran3Ma[1+((i+30)mod 55)];
        if Ran3Ma[i]<mz then Ran3Ma[i] := Ran3Ma[i]+mbig
      end
    end;
    Ran3Inext := 0;
    Ran3Inextp := 31;
    idum := 1
  end;
  Ran3Inext := Ran3Inext+1;
  if Ran3Inext=56 then Ran3Inext := 1;
  Ran3Inextp := Ran3Inextp+1;
  if Ran3Inextp=56 then Ran3Inextp := 1;
  mj := Ran3Ma[Ran3Inext]-Ran3Ma[Ran3Inextp];
  if mj<mz then mj := mj+mbig;
  Ran3Ma[Ran3Inext] := mj;
  ran3 := mj*fac
end; (* ran3 *)

function GasDev(var idum:integer):single;
(* Generate Gaussian deviates with Box-Muller method *)
var
  fac,r,v1,v2:single;
begin
  if GasdevIset=0 then begin
    repeat
      v1 := 2.0*ran3(idum)-1.0; //Use either ran0 or ran3
      v2 := 2.0*ran3(idum)-1.0;
      r := Sqr(v1)+Sqr(v2);
    until (r<1.0)and(r>0.0);
    fac := SqRt(-2.0*ln(r)/r);
    GasdevGset := v1*fac;
    GasDev := v2*fac;
    GasdevIset := 1
  end else begin
    GasdevIset := 0;
    GasDev := GasdevGset;
  end
end; (* GasDev *)

procedure four1(var data:N2array; nn,isign:integer);
(* Numerical Recipes, V1.1. Typen N2array skal have l�ngden 2*nn *)
var
  ii,jj,n,mmax,m,j,istep,i:integer;
  wtemp,wr,wpr,wpi,wi,theta:double;
  tempr,tempi:single;
begin
  n := 2*nn;
  j := 1;
  for ii := 1 to nn do begin
    i := 2*ii-1;
    if (j>i) then begin
      tempr := data[j];
      tempi := data[j+1];
      data[j] := data[i];
      data[j+1] := data[i+1];
      data[i] := tempr;
      data[i+1] := tempi
    end;
    m := n div 2;
    while ((m>=2)and(j>m)) do begin
      j := j-m;
      m := m div 2
    end;
    j := j+m
  end;
  mmax := 2;
  while (n>mmax) do begin
    istep := 2*mmax;
    theta := 6.28318530717959/(isign*mmax);
    wpr := -2.0*Sqr(sin(0.5*theta));
    wpi := sin(theta);
    wr := 1.0;
    wi := 0.0;
    for ii := 1 to(mmax div 2) do begin
      m := 2*ii-1;
      for jj := 0 to((n-m)div istep) do begin
        i := m+jj*istep;
        j := i+mmax;
        tempr := wr*data[j]-wi*data[j+1];
        tempi := wr*data[j+1]+wi*data[j];
        data[j] := data[i]-tempr;
        data[j+1] := data[i+1]-tempi;
        data[i] := data[i]+tempr;
        data[i+1] := data[i+1]+tempi
      end;
      wtemp := wr;
      wr := wr*wpr-wi*wpi+wr;
      wi := wi*wpr+wtemp*wpi+wi
    end;
    mmax := istep
  end
end; (* four1 *)

procedure RealFT(var data:N2array; n,isign:integer);
(* Numerical Recipes V1.1, typen N2array skal have laengden 2*n *)
var
  wr,wi,wpr,wpi,wtemp,theta:double;
  i,i1,i2,i3,i4:integer;
  c1,c2,h1r,h1i,h2r,h2i,wrs,wis:single;
begin
  theta := 6.28318530717959/(2.0*n);
  c1 := 0.5;
  if (isign=1) then begin
    c2 := -0.5;
    four1(data,n,1);
  end else begin
    c2 := 0.5;
    theta := -theta;
  end;
  wpr := -2.0*Sqr(sin(0.5*theta));
  wpi := sin(theta);
  wr := 1.0+wpr;
  wi := wpi;
  for i := 2 to(n div 2)+1 do begin
    i1 := i+i-1;
    i2 := i1+1;
    i3 := n+n+3-i2;
    i4 := i3+1;
    wrs := wr;
    wis := wi;
    h1r := c1*(data[i1]+data[i3]);
    h1i := c1*(data[i2]-data[i4]);
    h2r := -c2*(data[i2]+data[i4]);
    h2i := c2*(data[i1]-data[i3]);
    data[i1] := h1r+wrs*h2r-wis*h2i;
    data[i2] := h1i+wrs*h2i+wis*h2r;
    data[i3] := h1r-wrs*h2r+wis*h2i;
    data[i4] := -h1i+wrs*h2i+wis*h2r;
    wtemp := wr;
    wr := wr*wpr-wi*wpi+wr;
    wi := wi*wpr+wtemp*wpi+wi
  end;
  if (isign=1) then begin
    h1r := data[1];
    data[1] := h1r+data[2];
    data[2] := h1r-data[2]
  end else begin
    h1r := data[1];
    data[1] := c1*(h1r+data[2]);
    data[2] := c1*(h1r-data[2]);
    four1(data,n,-1)
  end
end; { RealFT }

procedure SkrivID(seed:integer; UT:single);
(* Skriver ID og diverse inputdata i starten af wake-fil *)
var
  I,I1,I2,I3,I4:word;
  ID,IA:array[1..5] of word;
  TXTch:string[40];
  SA:array[1..4] of single;
  IDSA:array[1..5] of string[2];
  shelp, str1, str2, str3:string[25];
begin
(* Beregning af IDentifikationskode *)
  Decodedate(Now,I1,I2,I3);
  ID[1] := I1 mod 100;
  ID[2] := I2;
  ID[3] := I3;
  DecodeTime(now,I1,I2,I3,I4);
  ID[4] := I1;
  ID[5] := I2;
  for I := 1 to 5 do begin
    Str(ID[I]:2,IDSA[I]);
    if IDSA[I,1]=' ' then IDSA[I,1] := '0'
  end;

  Tekst:= 'TI factors';
  while Length(Tekst)<40 do Tekst := Tekst+' ';
  IDstr := IDSA[3]+'.'+IDSA[2]+'.'+IDSA[1]+' '+IDSA[4]+':'+IDSA[5];
  str(Fti_v:5:3,str1);
  str(Fti_w:5:3,str2);
  Str(Seed:1,str3);
  Shelp := 'w='+str2+'v='+str1;
  Delete(Tekst,40-Length(shelp),40);
  Tekst := str3+shelp+Tekst; (* Include TI reduction info *)
  for I := 1 to 40 do
    if I<=Length(TEKST) then TXTch[I] := TEKST[I] else TXTch[I] := ' ';
  IA[1] := NS; (* No of stations = 2 *)
  IA[2] := NA; (* No of azimuths = 1 *)
  IA[3] := NP; (* Total no of points = 2 *)
  IA[4] := N2;  (*total time steps = 4096*)
  IA[5] := Ifak;
  SA[1] := DT; (* Time step *)
  SA[2] := UT; (* Transport speed *)
  SA[3] := 1; (* seed number*)
  SA[4] := 1;  (*length scale*)
  RS[1] := 0; (* Dummy radius *)
  RS[2] := 100; (* Dummy radius *)
  BlockWrite(F3,ID,5);
  BlockWrite(F3,TXTch,20);
  BlockWrite(F3,IA,5);
  BlockWrite(F3,RS,2*NS);
  BlockWrite(F3,SA,8);
{ TOTAL (5+20+5+10+8)*2 = 48*2 = 96 }
end; (* SkrivID *)

procedure Initialise(Seed:integer);
begin
(* initialisering af random-generator *)
  GasDevISet := 0;
  if Seed=0 then begin
    idum := 1; //Positive value
    Randomize;
  end else begin
    idum := -Seed;
  end;
end; (* Initialise *)

procedure GenerateSeries(D,U,Period:single);

  function Filter(D,f,U:single):single;
  { Low pass filter
    Note: filter on S(f), not on amplitude }
  begin
    result := 1/Sqr(1+Tau*D/U*2*Pi*f);
  end;

  function Sx(f,U:single):single;
  { Kaimal turbulence spectrum acc. to IEC 61400-1 vetical turbulence length}
  begin
    result := 4*Lkw/U/Power(1+6*f*Lkw/U,5/3);
  end;

  function Sy(f,U:single):single;
  { Kaimal turbulence spectrum acc. to IEC 61400-1 lateral turbulence length}
  begin
    result := 4*Lkv/U/Power(1+6*f*Lkv/U,5/3);
  end;

var
  I,J:integer;
  SumY,SumX,SumY_nf,SumX_nf,SumX_t:double;
  SigX,SigY,SigX_nf,SigY_nf: double;
  Ampx,Ampy,Ampx_nf,Ampy_nf,f,FIR,AbsMax,compX, compY:single;
begin
  for I := 1 to N2 do begin
    X[I] := 0; Y[I] := 0 ; FQ[I]:=0;
  end;

  if Period<=0 then begin
    X[1] := 0;
    X[2] := 0;
    Y[1] := 0; { Constant signal }
    Y[2] := 0; { Highest frequency }
(* Generate 2 phase angles or Gaussian deviates *)
    for I := 1 to N-1 do begin
      f := I*DF;
      FQ[I] := f;
      Ampx_nf := SqRt(2*Sx(f,U)); (* Not filtered *)
      Ampy_nf := SqRt(2*Sy(f,U));

      if f<Fc then begin
        Ampx := Ampx_nf; (* Filtered *)
        Ampy := Ampy_nf;
      end else begin
        Ampx := 0;
        Ampy := 0;
      end;

      if RanType='Gauss' then begin
        (* Gaussian random deviates *)
        X_nf[2*I+1] := GasDev(idum)*Ampx_nf;
        X_nf[2*I+2] := GasDev(idum)*Ampx_nf;
        X[2*I+1]    := X_nf[2*I+1]*Ampx/Ampx_nf;
        X[2*I+2]    := X_nf[2*I+2]*Ampx/Ampx_nf;

        Y_nf[2*I+1] := GasDev(idum)*Ampy_nf;
        Y_nf[2*I+2] := GasDev(idum)*Ampy_nf;
        Y[2*I+1]    := Y_nf[2*I+1]*Ampy/Ampy_nf;
        Y[2*I+2]    := Y_nf[2*I+2]*Ampy/Ampy_nf;
      end else begin(* Uniform *)
        (* Uniformly distributed phase angle, amplitude fixed *)
        FIR := ran0(idum)*2*pi;
        X[2*I+1] := Cos(FIR)*Ampx;
        X[2*I+2] := Sin(FIR)*Ampx;
        X_nf[2*I+1] := Cos(FIR)*Ampx_nf;
        X_nf[2*I+2] := Sin(FIR)*Ampx_nf;

        FIR := ran0(idum)*2*pi;
        Y[2*I+1] := Cos(FIR)*Ampy;
        Y[2*I+2] := Sin(FIR)*Ampy;
        Y_nf[2*I+1] := Cos(FIR)*Ampy_nf;
        Y_nf[2*I+2] := Sin(FIR)*Ampy_nf;
      end
    end;
  end else begin(* Generate a sine wave *)
    I := Round(1/period/df);
    Y[2*I+2] := 1; (* All other components zero *)
  end;

  //*********************** in frequency domain, no need of FFT; PSD will be square of these values / df.
  X4plot := X;
  Y4plot := Y;
  X_nf4plot := X_nf;
  Y_nf4plot := Y_nf;
  { Inverse Fourier transform }
  RealFT(X4plot,N,-1);
  RealFT(Y4plot,N,-1);
  RealFT(X_nf4plot,N,-1);
  RealFT(Y_nf4plot,N,-1);
  //***********************

(* Normalise to unity variance *)
  SumX := 0;
  SumY := 0;
  SumX_nf := 0;
  SumY_nf := 0;
  for I:=1 to N2 do begin
    SumX    := SumX    + X[I]*X[I];
    SumY    := SumY    + Y[I]*Y[I];
    SumX_nf := SumX_nf + X_nf[I]*X_nf[I];
    SumY_nf := SumY_nf + Y_nf[I]*Y_nf[I];
  end;
(* Variance, Var = Sum(sin^2+cos^2)/2; *)
  SumX := SumX/2;
  SumY := SumY/2;
  SumX_nf := SumX_nf/2; (* Approximately 1/DF *)
  SumY_nf := SumY_nf/2; (* Approximately 1/DF *)

  SigX := Sqrt(SumX);
  SigY := Sqrt(SumY);
  SigX_nf := Sqrt(SumX_nf);
  SigY_nf := Sqrt(SumY_nf);

  Fti_w := SumX/SumX_nf; // variance ratio: vertical (NOTE: Not stdev ratio)
  Fti_v := SumY/SumY_nf; // variance ratio: lateral

  (* Check: Must correspond to these integerals (ca) *)
  //compX :=  Fti_w/(1-Power(1+6*Lkw/(2*D),-2/3));
  //compY :=  Fti_v /(1-Power(1+6*Lkv/(2*D),-2/3));

  for I := 1 to N2 do begin
    if SumX>0 then X[I] := X[I]/SigX;
    if SumY>0 then Y[I] := Y[I]/SigY;
    if SumX_nf>0 then X_nf[I] := X_nf[I]/SigX_nf;
    if SumY_nf>0 then Y_nf[I] := Y_nf[I]/SigY_nf;
  end;

  //*************************** in frequency domain and normalised
  (*X4plot := X;
  Y4plot := Y;
  X_nf4plot := X_nf;
  Y_nf4plot := Y_nf; *)
  //****************************

{ Inverse Fourier transform }
  RealFT(X,N,-1);
  RealFT(Y,N,-1);
  RealFT(X_nf,N,-1);
  RealFT(Y_nf,N,-1);

  //****************************  in time domain and normalised
  (*X4plot := X;
  Y4plot := Y;
  X_nf4plot := X_nf;
  Y_nf4plot := Y_nf;  *)
  //****************************

  // variance check in time domain: Sum must be equal to length of signal
  SumX_t := 0;
  for I:=1 to N2 do begin
      SumX_t    := SumX_t    + X[I]*X[I];
  end;

{ Find constant for integer file }
  AbsMax := 0;
  for I := 1 to N2 do if Abs(X[I])>AbsMax then AbsMax := Abs(X[I]);
  for I := 1 to N2 do if Abs(Y[I])>AbsMax then AbsMax := Abs(Y[I]);
  if AbsMax=0 then IFAK := 1 else IFAK := Round(32000/AbsMax);
  for J := 1 to N2 do begin
    IntData[(J-1)*NP] := Round(X[J]*IFAK);
    IntData[(J-1)*NP+1] := Round(Y[J]*IFAK)
  end;
(* Repeat start points at the end *)
  IntData[N2*NP] := IntData[0];
  IntData[N2*NP+1] := IntData[1];
end; { GenerateSeries }

procedure ReadInputs;
  procedure ReadParams;
    begin
      SimFileName:= ParamStr(1);
    end;
var
  I:integer;
  Data : TJsonObject;
  pathFile_strArray  : TArray<string>;
begin
  ReadParams;
  Data:=TJsonObject.ParseFromFile(SimFileName) as TJsonObject;
  OutFileName:= Data['wake']['wakePathFile'];
  pathFile_strArray     := OutFileName.Split(['_']);
  D:= strtofloat(pathFile_strArray[1]);
  U:= strtofloat(pathFile_strArray[2]);
  Seed:= strtoint(pathFile_strArray[3]);
  RanType :=  'Gauss';
  Lkv:= 113.4;    // lateral
  Lkw:= 27.72;   // vertical
  Fc := U/(2*D);
  N2 := 4096;
  DT:= 0.15;
  sD := floattostr(Round(D));
  Tau:=  1.0;
  Nu:=  1;
  Ndir := 1;
  sDir := 1;
  NS := 2; (* fixed value, Y and Z *)
  NA := 1;
  NP := NA*NS;
(* diverse initialisering *)
  N := N2 div 2;
  DF := 1/(DT*N2);
  for I := 1 to NS do RS[I] := 0;

  (*FilterFileNameX:= 'DWM_sims\\'+floattostr(U)+'filterX.txt';
  NoFilterFileNameX:= 'DWM_sims\\'+floattostr(U)+'NofilterX.txt';
  FilterFileNameY:= 'DWM_sims\\'+floattostr(U)+'filterY.txt';
  NoFilterFileNameY:= 'DWM_sims\\'+floattostr(U)+'NofilterY.txt';
  freq :=  'DWM_sims\\'+floattostr(U)+'freq.txt';  *)
end; { ReadInputs }

var
  IU,IDIR,result,I:integer;
  strstr: string;
begin(* MAIN *)
  ReadInputs;
  SetLength(IntData,2*NP*N+2);
  Inc(Seed);
  Initialise(Seed);
  GenerateSeries(D,U,-1);
  OutFil := OutFileName;
  AssignFile(F3,OutFil);
  Rewrite(F3,2);
  SkrivID(Seed,U);
  BlockWrite(F3,IntData[0],NP*N2+2,Result);
  CloseFile(F3);
 /////////////////////////////to analyse filter //////////////////////////////////////////////////
  (*AssignFile(F4,FilterFileNameX );
  ReWrite(F4);
  for I := 1 to 4096 do begin
    WriteLn(F4, X4plot[I]:0:4);
  end;
  CloseFile(F4);

  AssignFile(F5,NoFilterFileNameX );
  ReWrite(F5);
  for I := 1 to 4096 do begin
    WriteLn(F5, X_nf4plot[I]:0:4);
  end;
  CloseFile(F5);

  AssignFile(F6,FilterFileNameY );
  ReWrite(F6);
  for I := 1 to 4096 do begin
    WriteLn(F6, Y4plot[I]:0:4);
  end;
  CloseFile(F6);

  AssignFile(F7,NoFilterFileNameY );
  ReWrite(F7);
  for I := 1 to 4096 do begin
    WriteLn(F7, Y_nf4plot[I]:0:4);
  end;
  CloseFile(F7);

  AssignFile(F8,freq );
  ReWrite(F8);
  for I := 1 to 4096 do begin
    WriteLn(F8, FQ[I]:0:4);
  end;
  CloseFile(F8);
  /////////////////////////////to analyse filter //////////////////////////////////////////////////*)
end.
