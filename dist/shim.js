#!/usr/bin/env node

const args = process.argv.slice(2);

let exec;
args.unshift(require('path').join(__dirname, require('./package.json').main));
if (process.platform === 'win32') {
    args.unshift('/C');
    exec = 'cmd';
} else {
    exec = 'wine';
    process.env.WINEDEBUG='-all';
}

require('child_process').spawn(exec, args, {stdio: 'inherit'})
    .on('error', function (err) {
        if (err.code === 'ENOENT' && err.path === 'wine') {
            console.error('Could not find wine, please install and try again (e.g. apt install wine)')
        } else {
            console.error(err);
        }
        process.exit(1);
    })
    .on('close', function (code) {
        process.exit(code);
    });