# wakemndr

wakemndr is an .int file generator for meandering wake path.

wakemndr is available through npm:

```
> npm install git+ssh://git@bitbucket.org/goldwinddk/wakemndr-win32.git
```